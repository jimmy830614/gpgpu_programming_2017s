#include <cstdio>
#include <cstdlib>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "SyncedMemory.h"


#define CHECK {\
	auto e = cudaDeviceSynchronize(); \
	if (e != cudaSuccess) {\
		printf("At " __FILE__ ":%d, %s\n", __LINE__, cudaGetErrorString(e)); \
		abort(); \
	}\
}
const int W = 40;
const int H = 12;



__global__ void Draw(char *frame) {
	// TODO: draw more complex things here
	// Do not just submit the original file provided by the TA!
	const int y = blockIdx.y * blockDim.y + threadIdx.y;
	const int x = blockIdx.x * blockDim.x + threadIdx.x;

	if (y < H && x < W) {
		char c;
		if (x == W - 1) {
			c = y == H - 1 ? '\0' : '\n';
		}
		else if (y == H - 2 && (x == W - 7 || (x <= W -19&& x>=W-32))){
			c = '#';
		}
		else if (y == H - 3 && ( (x <= W - 19 && x >= W - 30))){
			c = '#';
		}
		else if (y == H - 4 && ((x <= W - 19 && x >= W - 28))){
			c = '#';
		}
		else if (y == H - 5 && ((x <= W - 19 && x >= W - 26))){
			c = '#';
		}
		else if (y == H - 6 && ((x <= W - 19 && x >= W - 24))){
			c = '#';
		}
		else if (y == H - 7 && ((x <= W - 19 && x >= W - 22))){
			c = '#';
		}
		else if (y == H - 3 && x == W - 7){
			c = '|';
		}
		else if (y == H - 4 && x == W - 7){
			c = '|';
		}
		else if (y == H - 5 && x == W - 7){
			c = '|';
		}
		else if (y == H - 6 && x == W - 7){
			c = '|';
		}
		else if (y == H - 7 && x == W - 7){
			c = '|';
		}
		else if (y == H - 7 && x == W - 8){
			c = '<';
		}

		else if (y == 0 || y == H - 1 || x == 0 || x == W - 2) {
			c = ':';
		}
		else {
			c = ' ';
		}
		frame[y*W + x] = c;
	}
}

int main()
{
    /*const int arraySize = 5;
    const int a[arraySize] = { 1, 2, 3, 4, 5 };
    const int b[arraySize] = { 10, 20, 30, 40, 50 };
    int c[arraySize] = { 0 };*/

	char input;
	MemoryBuffer<char> frame(W*H);
	auto frame_smem = frame.CreateSync(W*H);
	CHECK;

	Draw << <dim3((W - 1) / 16 + 1, (H - 1) / 12 + 1), dim3(16, 12) >> >(frame_smem.get_gpu_wo());
	CHECK;

	puts(frame_smem.get_cpu_ro());
	CHECK;


	input = getchar();
    return 0;
}

// Helper function for using CUDA to add vectors in parallel.
